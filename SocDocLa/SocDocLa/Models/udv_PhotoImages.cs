//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SocDocLa.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class udv_PhotoImages
    {
        public int ImageID { get; set; }
        public Nullable<int> PhotoID { get; set; }
        public string ImageUrl { get; set; }
        public string ImageAlt { get; set; }
        public string Title { get; set; }
        public string Title_en { get; set; }
        public string Description { get; set; }
        public string Description_en { get; set; }
        public Nullable<System.DateTime> AddedDate { get; set; }
    
        public virtual udv_Photos udv_Photos { get; set; }
    }
}
