﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace SocDocLa.Models
{
    public class ArticlesModel
    {
        public int ArticleID { get; set; }

       [Required(ErrorMessage = "Bạn chưa nhập tiêu đề")]
        public string Title { get; set; }

        [Required(ErrorMessage = "Bạn chưa nhập tiêu đề tiếng anh")]
        public string Title_en { get; set; }

        [Required(ErrorMessage = "Bạn chưa nhập trích dẫn")]
        public string Abstract { get; set; }

        [Required(ErrorMessage = "Bạn chưa nhập trích dẫn en")]
        public string Abstract_en { get; set; }

        [Required(ErrorMessage = "Bạn chưa nhập nội dung chính")]
        public string Body { get; set; }

        [Required(ErrorMessage = "Bạn chưa nhập nội dung chính en")]
        public string Body_en { get; set; }

        [Required(ErrorMessage = "Bạn chọn mã thể loại")]
        public int CateID { get; set; }

        [Required(ErrorMessage = "Bạn chưa chọn url hình ảnh")]
        public string ImageUrl { get; set; }

        [Required(ErrorMessage = "Bạn chưa chọn alt hình ảnh")]
        public string ImageAlt { get; set; }

        [Required(ErrorMessage = "Bạn chưa chọn ngày phát hành")]
        public DateTime ReleaseDate { get; set; }

        [Required(ErrorMessage = "Bạn chưa chọn ngày hết hạn")]
        public DateTime ExpireDate { get; set; }

        [Required(ErrorMessage = "Bạn chưa nhập tình trạng")]
        public int StatusID { get; set; }

        [Required(ErrorMessage = "Bạn chưa bình luận")]
        public bool CommentsEnabled { get; set; }

        [Required(ErrorMessage = "Bạn chưa duyệt bình luận")]
        public bool ApprovedComments { get; set; }

        public bool OnlyForMembers { get; set; }

        public int TotalRating { get; set; }

        public int ViewCount { get; set; }

        public int DisplayOrder { get; set; }

        [Required(ErrorMessage = "Bạn chưa chọn ngày tạo")]
        public DateTime AddedDate { get; set; }

        [Required(ErrorMessage = "Bạn chưa chọn ngày cập nhật")]
        public DateTime UpdatedDate { get; set; }
        
        public string AddedBy { get; set; }

        public string UpdatedBy { get; set; }

        [Required(ErrorMessage = "Bạn chưa chọn kiểu duyệt")]
        public bool IsApproved { get; set; }

        [Required(ErrorMessage = "Bạn chưa nhập nội dung mô tả")]
        public string MetaDescription { get; set; }

        [Required(ErrorMessage = "Bạn chưa nhập nội dung keywords")]
        public string MetaKeywords { get; set; }

        [Required(ErrorMessage = "Bạn chưa nhập UrlRewrite")]
        public string UrlRewrite { get; set; }

        [Required(ErrorMessage = "Bạn chưa xác nhận đặt hàng")]
        public bool ConfirmOrder { get; set; }



    }
}