//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SocDocLa.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class udv_Users
    {
        public System.Guid UserID { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Gender { get; set; }
        public string BirthDay { get; set; }
        public string Address { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Mobile { get; set; }
        public string Fax { get; set; }
        public Nullable<int> ProvinceID { get; set; }
        public Nullable<int> CountryID { get; set; }
        public string PostalCode { get; set; }
        public string Company { get; set; }
        public string CompanyDescription { get; set; }
        public string EmployeesSize { get; set; }
        public string ImageUrl { get; set; }
        public string Website { get; set; }
        public Nullable<System.DateTime> AddedDate { get; set; }
        public string AddedBy { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<bool> Visible { get; set; }
    
        public virtual aspnet_Users aspnet_Users { get; set; }
    }
}
