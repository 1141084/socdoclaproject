﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace SocDocLa.Models
{
    public class QuanLyProductModel
    {

    }
    public class ProductModel
    {

        public int CateID { get; set; }

        public int ProductId { get; set; }

        public DateTime AddedDate { get; set; }

        public string AddedBy { get; set; }

        public DateTime UpdatedDate { get; set; }

        public string UpdatedBy { get; set; }

         [Required(ErrorMessage = "Bạn chưa nhập tên nhà sản xuất")]
        public string Manufacturer { get; set; }

         [Required(ErrorMessage = "Bạn chưa nhập mã sản phẩm")]
        public string ProductCode { get; set; }

        [Required(ErrorMessage = "Bạn chưa nhập tên sản phẩm")]
        public string Title { get; set; }

        [Required(ErrorMessage = "Bạn chưa nhập tên sản phẩm")]
        public string Title_en { get; set; }

        [Required(ErrorMessage = "Bạn chưa nhập mô tả")]
        public string Description { get; set; }

        [Required(ErrorMessage = "Bạn chưa nhập mô tả")]
        public string Description_en { get; set; }

        public int UnitInStock { get; set; }

         [Required(ErrorMessage = "Bạn chưa nhập giá gốc")]
        public Decimal UnitPrice { get; set; }

         [Required(ErrorMessage = "Bạn chưa nhập giá bán")]
        public Decimal SalePrice { get; set; }

         [Required(ErrorMessage = "Bạn chưa nhập giá khuyến mãi")]
        public Decimal PromotionPrice { get; set; }

         [Required(ErrorMessage = "Bạn chưa nhập phần trăm khuyến mãi")]
        public int PromotionPercent { get; set; }

        public string ImageUrl { get; set; }

        public string ImageAlt { get; set; }

         [Required(ErrorMessage = "Bạn chưa nhập vị trí")]
        public string DisplayOrder { get; set; }

        public int StatusID { get; set; }

         [Required(ErrorMessage = "Bạn chưa nhập thời gian bảo hành")]
        public int Warranty { get; set; }

        public Boolean IsActive { get; set; }

        public int TotalViews { get; set; }

        [Required(ErrorMessage = "Bạn chưa nhập mô tả chi tiết sản phẩm ")]
        public string Summary { get; set; }

        [Required(ErrorMessage = "Bạn chưa nhập mô tả chi tiết sản phẩm ")]
        public string Summary_en { get; set; }

        [Required(ErrorMessage = "Bạn chưa nhập mô tả tính năng ")]
        public string Feature { get; set; }

        [Required(ErrorMessage = "Bạn chưa nhập mô tả tính năng ")]
        public string Feature_en { get; set; }

        [Required(ErrorMessage = "Bạn chưa nhập kích thước ")]
        public string Size { get; set; }

        public DateTime ReleaseDate { get; set; }

        public DateTime ExpireDate { get; set; }

        public Boolean CommentsEnabled { get; set; }//cho p

        public Boolean ApprovedComments { get; set; }

        public Boolean OnlyForMembers { get; set; }

        public int TotalRating { get; set; }

        [Required(ErrorMessage = "Bạn chưa nhập mô tả ")]
        public string MetaDescription { get; set; }

        [Required(ErrorMessage = "Bạn chưa nhập từ khóa")]
        public string MetaKeywords { get; set; }

        public string UrlRewrite { get; set; }
    }

}