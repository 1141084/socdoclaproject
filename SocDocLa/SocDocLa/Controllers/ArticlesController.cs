﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Web;
using System.Web.Mvc;
using SocDocLa.Models;
using System.Web.UI.WebControls;

namespace SocDocLa.Controllers
{
    public class ArticlesController : Controller
    {
        //
        // GET: /Articles/
        udv_Articles baiViet = new udv_Articles();
        SocDocLaEntities sdl = new SocDocLaEntities();
        public ActionResult Index(string Search, int? MaDanhMuc)
        {
            var lst = sdl.udv_Articles.Where(d =>
                ((!string.IsNullOrEmpty(Search) && d.Title.Contains(Search)) || string.IsNullOrEmpty(Search)) &&
                ((d.CateID == (MaDanhMuc ?? 0) && (MaDanhMuc ?? 0) != 0) || (MaDanhMuc ?? 0) == 0)
                );
            ViewBag.maDanhMucId = new SelectList(sdl.udv_Categories, "CateID", "Title", "");
            ViewData["maDanhMuc"] = MaDanhMuc ?? 0;
            ViewData["search"] = Search;
            return View("Index", lst);
        }

        public ActionResult AddNewArticles()
        {
            ViewBag.Categories = sdl.udv_Categories.OrderBy(d => d.Title).ToList();
            return View();
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult AddNewArticles(ArticlesModel acticlesModel, int dropdownDanhMuc)
        {
            try
            {
                if (acticlesModel.Title_en == "" || acticlesModel.Title_en == "" || acticlesModel.Abstract == "" || acticlesModel.Abstract_en == ""
                    || acticlesModel.MetaKeywords == "" || acticlesModel.MetaDescription == "" || acticlesModel.Body == "" || acticlesModel.Body_en == ""
                    || acticlesModel.DisplayOrder == 0)
                {
                    ViewBag.Categories = sdl.udv_Categories.OrderBy(d => d.Title).ToList();
                    return View("AddNewArticles");
                }
                else
                {
                    UploadFileToServer();
                    baiViet.Title = acticlesModel.Title;
                    baiViet.Title_en = acticlesModel.Title_en;
                    baiViet.Abstract = acticlesModel.Abstract;
                    baiViet.CateID = dropdownDanhMuc;
                    baiViet.Abstract_en = acticlesModel.Abstract_en;
                    baiViet.MetaDescription = acticlesModel.MetaDescription;
                    baiViet.MetaKeywords = acticlesModel.MetaKeywords;
                    baiViet.Body = acticlesModel.Body;
                    baiViet.Body_en = acticlesModel.Body_en;
                    baiViet.DisplayOrder = Convert.ToInt32(acticlesModel.DisplayOrder);
                    baiViet.IsApproved = acticlesModel.IsApproved;
                    baiViet.CommentsEnabled = acticlesModel.CommentsEnabled;
                    baiViet.OnlyForMembers = acticlesModel.OnlyForMembers;
                    baiViet.ConfirmOrder = acticlesModel.ConfirmOrder;
                    baiViet.ApprovedComments = acticlesModel.ApprovedComments;
                    baiViet.AddedDate = DateTime.Now;
                    //string ngayPhat=acticlesModel.ReleaseDate.ToString();
                    //string ngayHet=acticlesModel.ExpireDate.ToString();
                    //DateTime ngayPhatHanh = DateTime.Parse(ConvertDate(ngayPhat));
                    //DateTime ngayHetHan = DateTime.Parse(ConvertDate(ngayHet));
                    //baiViet.ReleaseDate = ngayPhatHanh;
                    //baiViet.ExpireDate = ngayHetHan;
                    sdl.udv_Articles.Add(baiViet);
                    sdl.SaveChanges();

                    return RedirectToAction("Index", "Articles");
                }
            }
            catch (Exception ex)
            {
                ViewBag.Categories = sdl.udv_Categories.OrderBy(d => d.Title).ToList();
                return View();
            }
        }

        public string ConvertDate(string str)
        {
            string[] strDateTime = str.Split(',');
            string[] strDate = strDateTime[1].Split('/');
            string date = strDate[1] + "/" + strDate[0] + "/" + strDate[2];
            return date;
        }

        /// <summary>
        /// Upload file to server. Neu chua ton tai thu muc thi tu tao thu muc
        /// </summary>
        /// <summary>
        /// Upload file to server. Neu chua ton tai thu muc thi tu tao thu muc
        /// </summary>
        private void UploadFileToServer()
        {
            try
            {
                string fileNameDK = string.Empty;
                string filePathSave = string.Empty;
                foreach (string inputTagName in Request.Files)
                {
                    HttpPostedFileBase file = Request.Files[inputTagName];
                    if (file.ContentLength > 0)
                    {
                        //OK
                        string dateNow = DateTime.Now.ToString("yyyyMMddhhmmss");
                        fileNameDK = Path.GetFileNameWithoutExtension(file.FileName) + Path.GetExtension(file.FileName);
                        string fileName = Path.GetFileNameWithoutExtension(file.FileName) + "_" + dateNow;
                        string fileExtend = Path.GetExtension(file.FileName);
                        filePathSave = fileName + fileExtend;

                        string Duongdan = Server.MapPath("~/Content/Images/");
                        if (!Directory.Exists(Duongdan))
                        {
                            Directory.CreateDirectory(Duongdan);
                        }
                        string filePath = Path.Combine(Duongdan, filePathSave);
                        file.SaveAs(filePath);
                    }
                    //sanPham.ImageAlt = filePathSave;
                    baiViet.ImageUrl = filePathSave;
                }
            }
            catch
            {
            }
        }

        //delete bài viết
        [HttpPost]
        public ActionResult Delete(int[] chkId)
        {
            try
            {
                for (int i = 0; i < chkId.Length; i++)
                {
                    int temp = chkId[i];
                    var articleId = sdl.udv_Articles.Where(d => d.ArticleID == temp).SingleOrDefault();
                    sdl.udv_Articles.Remove(articleId);
                    sdl.SaveChanges();
                }
            }
            catch (Exception ex)
            {
            }
            return RedirectToAction("Index", "Articles");
        }

    }
}
