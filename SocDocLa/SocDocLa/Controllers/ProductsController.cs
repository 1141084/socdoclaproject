﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Web;
using System.Web.Mvc;
using SocDocLa.Models;
using System.Web.UI.WebControls;
namespace SocDocLa.Controllers
{
    public class ProductsController : Controller
    {
        udv_Products sanPham = new udv_Products();
        //
        // GET: /Products/
        SocDocLaEntities sdl = new SocDocLaEntities();
        public ActionResult Index(string Search, int? MaDanhMuc)
        {
            var lst = sdl.udv_Products.Where(d =>
                ((!string.IsNullOrEmpty(Search) && d.Title.Contains(Search)) || string.IsNullOrEmpty(Search)) &&
                ((d.CateID == (MaDanhMuc ?? 0) && (MaDanhMuc ?? 0) != 0) || (MaDanhMuc ?? 0) == 0)
                );
            ViewBag.maDanhMucId = new SelectList(sdl.udv_Categories, "CateID", "Title", "");
            ViewData["maDanhMuc"] = MaDanhMuc ?? 0;
            ViewData["search"] = Search;
            return View("Index", lst);
        }

        //public ActionResult SearchDanhMuc(string maDanhMuc)
        //{
        //    var data = sdl.udv_Products.Where(d => (!string.IsNullOrEmpty(maDanhMuc) && d.CateID.Equals(maDanhMuc)) || string.IsNullOrEmpty(maDanhMuc));
        //    ViewBag.maDanhMucId = new SelectList(sdl.udv_Categories, "CateID", "Title", maDanhMuc);
        //    return View("Index",data);
        //}
            

        //public ActionResult TimKiem(string Search)
        //{
        //    var lst = sdl.udv_Products.Where(d => d.Title.Equals(Search)).ToList();
        //    ViewBag.maDanhMucId = new SelectList(sdl.udv_Categories, "CateID", "Title", "");
        //    return View("Index",lst);
        //}

        public ActionResult AddNewProduct()
        {
            //ViewBag.maDanhMucId = new SelectList(sdl.udv_Categories, "CateID", "Title", "");  
            ViewBag.Categories = sdl.udv_Categories.OrderBy(d => d.Title).ToList();
            return View();
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult AddNewProduct(ProductModel collection, int dropdownDanhMuc)
        {
            try
            {
                if (collection.Title == "" || dropdownDanhMuc == 0 || collection.ProductCode == "" || collection.Manufacturer == ""
                    || collection.UnitPrice == 0 || collection.SalePrice == 0 || collection.PromotionPrice == 0 || collection.PromotionPercent == 0 ||
                    collection.Warranty == 0 || collection.Description == "" || collection.Description_en == ""
                    || collection.Feature == "" || collection.Feature_en == "" || collection.MetaDescription == "" || collection.MetaKeywords == ""
                    || collection.DisplayOrder == "" || collection.Size == "" || collection.Summary=="" || collection.Summary_en=="")
                {
                    //var errors = ModelState.SelectMany(x => x.Value.Errors.Select(z => z.Exception));
                    //ViewBag.maDanhMucId = new SelectList(sdl.udv_Categories, "CateID", "Title", "");
                    ViewBag.Categories = sdl.udv_Categories.OrderBy(d => d.Title).ToList();
                    return View("AddNewProduct");
                }
                else
                {

                    UploadFileToServer();
                    sanPham.Title = collection.Title;
                    sanPham.ImageAlt = collection.Title;
                    sanPham.Title_en = collection.Title_en;
                    sanPham.AddedDate = DateTime.Now;
                    sanPham.CateID = dropdownDanhMuc;
                    sanPham.ProductCode = collection.ProductCode;
                    sanPham.Manufacturer = collection.Manufacturer;
                    sanPham.UnitPrice = collection.UnitPrice;
                    sanPham.Summary = collection.Summary;
                    sanPham.Summary_en = collection.Summary_en;
                    sanPham.SalePrice = collection.SalePrice;
                    sanPham.Size = collection.Size;
                    sanPham.PromotionPrice = collection.PromotionPrice;
                    sanPham.PromotionPercent = collection.PromotionPercent;
                    sanPham.Warranty = collection.Warranty;
                    sanPham.Description = collection.Description;
                    sanPham.Description_en = collection.Description_en;
                    sanPham.Feature = collection.Feature;

                    sanPham.CommentsEnabled = collection.CommentsEnabled;
                    sanPham.OnlyForMembers = collection.OnlyForMembers;
                    sanPham.ApprovedComments = collection.ApprovedComments;
                    sanPham.IsActive = collection.IsActive;
                    sanPham.Feature_en = collection.Feature_en;
                    sanPham.MetaDescription = collection.MetaDescription;
                    sanPham.MetaKeywords = collection.MetaKeywords;
                    sanPham.DisplayOrder = Convert.ToInt32(collection.DisplayOrder);
                    sdl.udv_Products.Add(sanPham);
                    sdl.SaveChanges();
                    return RedirectToAction("Index", "Products");
                }

            }
            catch (Exception ex)
            {
                //                ViewBag.maDanhMucId = new SelectList(sdl.udv_Categories, "CateID", "Title", "");  
                ViewBag.Categories = sdl.udv_Categories.OrderBy(d => d.Title).ToList();
                return View();
            }
        }
        /// <summary>
        /// Upload file to server. Neu chua ton tai thu muc thi tu tao thu muc
        /// </summary>
        /// <summary>
        /// Upload file to server. Neu chua ton tai thu muc thi tu tao thu muc
        /// </summary>
        private void UploadFileToServer()
        {
            try
            {
                string fileNameDK = string.Empty;
                string filePathSave = string.Empty;
                foreach (string inputTagName in Request.Files)
                {
                    HttpPostedFileBase file = Request.Files[inputTagName];
                    if (file.ContentLength > 0)
                    {
                        //OK
                        string dateNow = DateTime.Now.ToString("yyyyMMddhhmmss");
                        fileNameDK = Path.GetFileNameWithoutExtension(file.FileName) + Path.GetExtension(file.FileName);
                        string fileName = Path.GetFileNameWithoutExtension(file.FileName) + "_" + dateNow;
                        string fileExtend = Path.GetExtension(file.FileName);
                        filePathSave = fileName + fileExtend;

                        string Duongdan = Server.MapPath("~/Content/Images/");
                        if (!Directory.Exists(Duongdan))
                        {
                            Directory.CreateDirectory(Duongdan);
                        }
                        string filePath = Path.Combine(Duongdan, filePathSave);
                        file.SaveAs(filePath);                       
                    }
                    //sanPham.ImageAlt = filePathSave;
                    sanPham.ImageUrl = filePathSave;                   
                }
            }
            catch
            {                
            }            
        }

        //delete san pham
        [HttpPost]
        public ActionResult Delete(int [] chkId)
        {
            try
            {
                for (int i = 0; i < chkId.Length;i++ )
                {
                    int temp = chkId[i];
                    var productId = sdl.udv_Products.Where(d=>d.ProductID==temp).SingleOrDefault();
                    sdl.udv_Products.Remove(productId);
                    sdl.SaveChanges();
                }
            }
            catch(Exception ex)
            {                
            }
            return RedirectToAction("Index", "Products");
        }
    }
}
